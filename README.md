# Start using

1. Clone the repository
2. Open the repository in your favorite IDE

## Usage 
Run the `com.hotels.exercise.SpringBootSampleApplication`

You now have a sample Spring Boot application running.

## End points

`http://localhost:8080/hello` will respond with `Hello world!`

# Legal
This project is available under the [Apache 2.0 License](http://www.apache.org/licenses/LICENSE-2.0.html).

Copyright 2016-2018 Expedia Inc.